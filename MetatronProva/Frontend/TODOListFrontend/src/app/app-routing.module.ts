import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { MainComponent } from './pages/main/main.component';
import { CadastroComponent } from './pages/cadastro/cadastro.component';
import { TodolistComponent } from './pages/todolist/todolist.component';


const routes: Routes = [    
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent, data: { title: 'TODO List Login' } },
  { path: 'cadastro', component: CadastroComponent, data: { title: 'Cadastro de Usuário' } },
  { path: 'main', component: MainComponent, children: [
      { path: '', redirectTo: 'todolist', pathMatch: 'full' },
      { path: 'todolist', component: TodolistComponent, data: { title: 'TODO List' }, pathMatch: 'full' },      
    ]
  }  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
