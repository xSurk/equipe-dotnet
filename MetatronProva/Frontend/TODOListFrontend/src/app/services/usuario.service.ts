import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import APIURL from '../app.api';
import { Usuario } from '../classes/Usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) { }

  public Add(usuario){
    return this.http.post(APIURL + 'usuario', usuario)    
  }

  public List(){
    return this.http.get(APIURL + 'usuario')    
  }

  public Get(id: string){
    return this.http.get(APIURL + 'usuario' + '/' + id)    
  }

  public Update(usuario){
    return this.http.put(APIURL + 'usuario', usuario.id)    
  }

  public Delete(usuario) {
    return this.http.delete(APIURL + 'usuario', usuario.id)
  }

  public Login(usuario) {
    return this.http.request("GET", APIURL + 'usuario/Login', {responseType: "json", params: { nome: usuario.nome, senha: usuario.senha } })
  }
}
