import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import APIURL from '../app.api';

@Injectable({
  providedIn: 'root'
})
export class TarefasService {

  headers = new HttpHeaders();

  constructor(private http: HttpClient) {
    this.headers.append('Content-Type','application/json');
  }

  public Add(tarefa){
    return this.http.post(APIURL + 'tarefa', tarefa, {headers: this.headers})    
  }

  public List(){
    return this.http.get(APIURL + 'tarefa', {headers: this.headers})    
  }

  public Get(id: string){
    return this.http.get(APIURL + 'tarefa' + '/' + id)    
  }

  public Update(tarefa){    
    return this.http.put(APIURL + 'tarefa', tarefa, {headers: this.headers})    
  }

  public Delete(tarefa) {    
    let httpParams = new HttpParams().set('id', tarefa.id)
    let options = { headers: this.headers, params: httpParams }
    
    return this.http.get(APIURL + 'tarefa/Delete/' + tarefa.id, {headers: this.headers})
  }
}
