import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import APIURL from '../app.api';

@Injectable({
  providedIn: 'root'
})
export class ListaService {

  headers = new HttpHeaders();

  constructor(private http: HttpClient) {
    this.headers.append('Content-Type','application/json');
  }

  public Add(lista){
    return this.http.post(APIURL + 'lista', lista, {headers: this.headers})    
  }

  public List(){
    return this.http.get(APIURL + 'lista', {headers: this.headers})    
  }

  public Get(id: string){
    return this.http.get(APIURL + 'lista' + '/' + id)    
  }

  public Update(lista){
    console.log('Lista a ser alterada: ', lista)
    return this.http.put(APIURL + 'lista', lista, {headers: this.headers})    
  }

  public Delete(listaId) {    
    // let httpParams = new HttpParams().set('id', listaId)
    // let options = { headers: this.headers, params: httpParams }

    return this.http.delete(APIURL + 'lista/Delete/' + listaId, {headers: this.headers})
  }
}
