import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import APIURL from '../app.api';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  headers = new HttpHeaders();

  constructor(private http: HttpClient) {
    this.headers.append('Content-Type','application/json');
  }

  public Add(categoria){
    return this.http.post(APIURL + 'categoria', categoria) //{headers: this.headers}    
  }

  public List(){
    return this.http.get(APIURL + 'categoria')    
  }

  public Get(id: string){
    return this.http.get(APIURL + 'categoria' + '/' + id)    
  }

  public Update(categoria){
    return this.http.put(APIURL + 'categoria', categoria.id)    
  }

  public Delete(categoria) {
    return this.http.delete(APIURL + 'categoria', categoria.id)
  }
}
