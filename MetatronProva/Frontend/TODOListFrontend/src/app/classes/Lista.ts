import { Categoria } from './Categoria';
import { Tarefa } from './Tarefa';

export class Lista {
    id:number;
    nome:string;
    categoriaId: number;
    concluida:boolean;
    Categoria?: Categoria;
    Tarefas?: Tarefa[];
    constructor(id:number,nome:string,categoriaId:number,concluida:boolean){
        this.id = id
        this.nome = nome
        this.categoriaId = categoriaId
        this.concluida = concluida
    }
}