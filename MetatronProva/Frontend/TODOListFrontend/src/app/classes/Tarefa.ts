import { Usuario } from './Usuario';
import { Lista } from './Lista';

export class Tarefa {
    id:number;
    listaId: number;
    nome:string;
    concluida:boolean;
    usuarioId: number;
    constructor(id:number,listaId:number,nome:string,concluida:boolean,usuarioId:number){
        this.id = id
        this.listaId = listaId
        this.nome = nome
        this.concluida = concluida
        this.usuarioId = usuarioId
    }
}