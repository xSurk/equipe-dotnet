import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Categoria } from 'src/app/classes/Categoria';
import { Lista } from 'src/app/classes/Lista';
import { CategoriaService } from 'src/app/services/categoria.service';

@Component({
  selector: 'app-listadialog',
  templateUrl: './listadialog.component.html',
  styleUrls: ['./listadialog.component.css']
})
export class ListadialogComponent implements OnInit {
  categorias: Categoria[]

  constructor(
    private categoriaService: CategoriaService,
    public dialogRef: MatDialogRef<ListadialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Lista = new Lista(0,'',0,false)) { }

  ngOnInit(): void {
    this.categoriaService.List().subscribe((data) => {
      console.log('Categorias: ', data)
      this.categorias = <Categoria[]>data
    })
  }

  onNoClick(): void {        
    this.dialogRef.close();
  }

  // selectOnChange(){
  //   console.log('categoria: ', this.categorias.find(x => x.id == <number><unknown>this.data.categoriaId))
  //   this.data.categoria = this.categorias.find(x => x.id == <number><unknown>this.data.categoriaId)
  // }

}
