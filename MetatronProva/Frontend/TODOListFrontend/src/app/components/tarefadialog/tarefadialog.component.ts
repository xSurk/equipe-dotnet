import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Tarefa } from 'src/app/classes/Tarefa';

@Component({
  selector: 'app-tarefadialog',
  templateUrl: './tarefadialog.component.html',
  styleUrls: ['./tarefadialog.component.css']
})
export class TarefadialogComponent implements OnInit {

  constructor(
    // private categoriaService: CategoriaService,
    public dialogRef: MatDialogRef<TarefadialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Tarefa = new Tarefa(0,0,'',false,0)) { }

  ngOnInit(): void {    
  }

  onNoClick(): void {        
    this.dialogRef.close();
  }

}
