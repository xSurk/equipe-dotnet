import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Categoria } from 'src/app/classes/Categoria';

@Component({
  selector: 'app-categoriadialog',
  templateUrl: './categoriadialog.component.html',
  styleUrls: ['./categoriadialog.component.css']
})
export class CategoriadialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CategoriadialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Categoria = new Categoria(0,'')
    ) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
