import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { Usuario } from '../../classes/Usuario';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {  
  usuario: Usuario = new Usuario(0,'','','')

  constructor(private userService: UsuarioService,
              private _snackBar: MatSnackBar,
              private route: Router) {    
  }

  ngOnInit(): void {
    this.userService.Get('1').subscribe((data) => {
      console.log(data)
    })
  }

  Login(){
    this.userService.Login(this.usuario).subscribe((data) =>{  
      console.log(data)    
      if (data)
        this.route.navigateByUrl('main')

      else
        this.abrirSnackBar('Falha ao efetuar login', 'Error!')
    })    
  }

  abrirSnackBar(mensagem: string, acao: string){
    this._snackBar.open(mensagem, acao, {
      duration: 5000
    })
  }

}
