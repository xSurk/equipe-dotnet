import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { Usuario } from '../../classes/Usuario';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {
  usuarios: Usuario[]
  usuario: Usuario = new Usuario(0,'','','')

  constructor(private userService: UsuarioService,
              private _snackBar: MatSnackBar,
              private route: Router) { }

  ngOnInit(): void {
  }

  cadastrarUsuario(){ 
    if(!this.validarUsuario()) return

    this.userService.Add(this.usuario).subscribe((data) =>{      
      if(data)
        this.abrirSnackBar('Falha ao efetuar cadastro', 'Error!') 
      else
        this.abrirSnackBar('Cadastro realizado com sucesso', 'Sucesso!')
        this.route.navigateByUrl('main')
    })
  }

  abrirSnackBar(mensagem: string, acao: string){
    this._snackBar.open(mensagem, acao, {
      duration: 5000
    })
  }

  validarUsuario(): boolean{
    if(this.usuario.nome.trim() == "") {
      this.abrirSnackBar('Favor fornecer um nome', 'Aviso!')
      return false
    }
    else if(this.usuario.email.trim() == "" || this.usuario.email.indexOf('@') == -1){
      this.abrirSnackBar('Email inválido', 'Aviso!')
      return false
    }
    else if(this.usuario.senha.trim() == ""){      
      this.abrirSnackBar('Favor fornecer uma senha', 'Aviso!')
      return false
    }
    else{
      return true
    }
  }

}
