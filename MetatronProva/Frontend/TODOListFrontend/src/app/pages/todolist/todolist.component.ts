import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ListadialogComponent } from 'src/app/components/listadialog/listadialog.component';
import { Lista } from 'src/app/classes/Lista';
import { CategoriadialogComponent } from 'src/app/components/categoriadialog/categoriadialog.component';
import { Categoria } from 'src/app/classes/Categoria';
import { CategoriaService } from 'src/app/services/categoria.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ListaService } from 'src/app/services/lista.service';
import { TarefadialogComponent } from 'src/app/components/tarefadialog/tarefadialog.component';
import { Tarefa } from 'src/app/classes/Tarefa';
import { TarefasService } from 'src/app/services/tarefas.service';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodolistComponent implements OnInit {
  lista: Lista = new Lista(0, '', 0, false);
  categoria: Categoria = new Categoria(0, '');
  tarefa: Tarefa = new Tarefa(0, 0, '', false, 0);

  listas: Lista[]
  categorias: Categoria[]
  tarefas: Tarefa[]

  filtroLista: any
  filtroCategoria: any

  constructor(public dialog: MatDialog,
              private categoriaService: CategoriaService,
              private listaService: ListaService,
              private tarefaService: TarefasService,
              private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.consultarListas()    
  }

  novaLista(){
    let dialogRef = this.dialog.open(ListadialogComponent, {
      width: '320px',
      data: {nome: this.lista.nome, categoriaId: this.lista.categoriaId}
    });

    dialogRef.afterClosed().subscribe(result => {              
      this.listaService.Add(result).subscribe((data) => {  
        this.consultarListas()      
        this.abrirSnackBar('Lista cadastrada com sucesso', 'Sucesso!')
      })
    });
  }

  novaCategoria(){
    let dialogRef = this.dialog.open(CategoriadialogComponent, {
      width: '320px',
      data: this.categoria
    });

    dialogRef.afterClosed().subscribe(result => {                  
      this.categoriaService.Add(result).subscribe((data) => {
        this.abrirSnackBar('Categoria cadastrada com sucesso', 'Sucesso!')
      })
    });
  }

  novaTarefa(lista){    
    let dialogRef = this.dialog.open(TarefadialogComponent, {
      width: '320px',
      data: new Tarefa(0, lista.id, '', false, 0)
    });

    dialogRef.afterClosed().subscribe(result => {                  
      this.tarefaService.Add(result).subscribe((data) => {
        this.consultarListas()
        this.abrirSnackBar('Tarefa cadastrada com sucesso', 'Sucesso!')
      })
    });
  }

  abrirSnackBar(mensagem: string, acao: string){
    this._snackBar.open(mensagem, acao, {
      duration: 5000
    })
  }

  consultarListas(){   
    this.consultarCategorias()
    this.consultarTarefas()

    setTimeout(() => {
      this.listaService.List().subscribe((data)=> {        
        (<Lista[]>data).forEach(x => {             
          x.Categoria = this.categorias.find(y => y.id == x.categoriaId)
          // x.Tarefas ? x.Tarefas : x.Tarefas = new Array()
          let tarefaNaLista = this.tarefas.filter(z => z.listaId == x.id)          
          // tarefaNaLista ? x.Tarefas = new Tarefa[]().push(tarefaNaLista) : x.Tarefas
          // console.log('Tarefa: ', tarefaNaLista)
          // if(tarefaNaLista){
          //   // x.Tarefas = new Array()
          //   // x.Tarefas.push(tarefaNaLista)

          //   x.Tarefas = tarefaNaLista
          // }          
          x.Tarefas = tarefaNaLista
        })
        this.listas = <Lista[]>data
      })
    }, 1000)    
  }

  consultarCategorias(){
    this.categoriaService.List().subscribe((data) => {      
      this.categorias = <Categoria[]>data
    })
  }

  consultarTarefas(){
    this.tarefaService.List().subscribe((data) => {      
      this.tarefas = <Tarefa[]>data
    })
  }

  alterarStatusTarefa(tarefa, event){    
    tarefa.concluida = event.checked    
    this.tarefaService.Update(tarefa).subscribe((data) => {
      this.consultarListas()
    })
  }

  alterarNomeTarefa(listaId, tarefa){
    let dialogRef = this.dialog.open(TarefadialogComponent, {
      width: '320px',
      data: new Tarefa(tarefa.id, listaId, tarefa.nome, tarefa.concluida, 0)
    });

    dialogRef.afterClosed().subscribe(result => {                  
      this.tarefaService.Update(result).subscribe((data) => {
        this.consultarListas()
        this.abrirSnackBar('Tarefa alterada com sucesso', 'Sucesso!')
      })
    });
  }

  deletarTarefa(tarefa) {
    this.tarefaService.Delete(tarefa).subscribe((data) => {
      this.consultarListas()
      this.abrirSnackBar('Tarefa deletada com sucesso', 'Sucesso!')
    })
  }

  alterarNomeLista(lista){
    let dialogRef = this.dialog.open(TarefadialogComponent, {
      width: '320px',
      data: new Lista(lista.id, lista.nome, lista.categoriaId, lista.concluida)
    });

    dialogRef.afterClosed().subscribe(result => {                  
      this.listaService.Update(result).subscribe((data) => {
        this.consultarListas()
        this.abrirSnackBar('Lista alterada com sucesso', 'Sucesso!')
      })
    });
  }

  deletarLista(listaId){
    this.listaService.Delete(listaId).subscribe((data) => {
      this.consultarListas()
      this.abrirSnackBar('Lista deletada com sucesso', 'Sucesso!')
    })
  }

  alterarStatusLista(lista, event){    
    lista.concluida = event.checked    
    this.listaService.Update(lista).subscribe((data) => {
      this.consultarListas()
    })
  }

  filtrarCampos(){   
    console.log('filtro cat: ', this.filtroCategoria)
    if((this.filtroCategoria == '' || this.filtroCategoria == undefined) && (this.filtroLista == undefined || this.filtroLista == '')) this.consultarListas()              
    if(this.filtroLista) this.listas = this.listas.filter(x => x.nome == this.filtroLista)    
    if(this.filtroCategoria) this.listas = this.listas.filter(x => x.Categoria.id.toString() == this.filtroCategoria) 
  }

}
