﻿using Application.Interfaces;
using Domain.Entities;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Applications
{
    public class ApplicationLista : IAppLista
    {
        ILista _lista;

        public ApplicationLista(ILista ILista)
        {
            _lista = ILista;
        }

        public void Add(Lista Entity)
        {
            _lista.Add(Entity);
        }

        public void Delete(int id)
        {
            _lista.Delete(id);
        }

        public Lista Get(int Id)
        {
            return _lista.Get(Id);
        }

        public List<Lista> List()
        {
            return _lista.List();
        }

        public void Update(Lista Entity)
        {
            _lista.Update(Entity);
        }
    }
}
