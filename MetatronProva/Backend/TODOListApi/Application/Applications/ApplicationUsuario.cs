﻿using Application.Interfaces;
using Domain.Entities;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Applications
{
    public class ApplicationUsuario : IAppUsuario
    {

        IUsuario _usuario;

        public ApplicationUsuario(IUsuario IUsuario)
        {
            _usuario = IUsuario;
        }

        public void Add(Usuario Entity)
        {
            _usuario.Add(Entity);
        }

        public void Delete(int id)
        {
            _usuario.Delete(id);
        }

        public Usuario Get(int Id)
        {
            return _usuario.Get(Id);
        }

        public List<Usuario> List()
        {
            return _usuario.List();
        }

        public void Update(Usuario Entity)
        {
            _usuario.Update(Entity);
        }

        public Usuario Login(string nome, string senha)
        {
            return _usuario.Login(nome, senha);
        }
    }
}
