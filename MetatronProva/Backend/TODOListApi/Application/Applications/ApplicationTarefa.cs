﻿using Application.Interfaces;
using Domain.Entities;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Applications
{
    public class ApplicationTarefa : IAppTarefa
    {
        ITarefa _tarefa;

        public ApplicationTarefa(ITarefa ITarefa)
        {
            _tarefa = ITarefa;
        }

        public void Add(Tarefa Entity)
        {
            _tarefa.Add(Entity);
        }

        public void Delete(int id)
        {
            _tarefa.Delete(id);
        }

        public Tarefa Get(int Id)
        {
            return _tarefa.Get(Id);
        }

        public List<Tarefa> List()
        {
            return _tarefa.List();
        }

        public void Update(Tarefa Entity)
        {
            _tarefa.Update(Entity);
        }
    }
}
