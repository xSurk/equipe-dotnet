﻿using Application.Interfaces;
using Domain.Entities;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Applications
{
    public class ApplicationCategoria : IAppCategoria
    {
        ICategoria _categoria;

        public ApplicationCategoria(ICategoria ICategoria)
        {
            _categoria = ICategoria;
        }

        public void Add(Categoria Entity)
        {
            _categoria.Add(Entity);
        }

        public void Delete(int id)
        {
            _categoria.Delete(id);
        }

        public Categoria Get(int Id)
        {
            return _categoria.Get(Id);
        }

        public List<Categoria> List()
        {
            return _categoria.List();
        }

        public void Update(Categoria Entity)
        {
            _categoria.Update(Entity);
        }
    }
}
