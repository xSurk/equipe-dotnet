﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interfaces
{
    public interface IAppLista : IAppGeneric<Lista>
    {
        void Delete(int id);
    }
}
