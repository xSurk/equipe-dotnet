﻿using Domain.Entities;

namespace Application.Interfaces
{
    public interface IAppUsuario : IAppGeneric<Usuario>
    {
        Usuario Login(string nome, string senha);
        void Delete(int id);
    }
}
