﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interfaces
{
    public interface IAppCategoria : IAppGeneric<Categoria>
    {
        void Delete(int id);
    }
}
