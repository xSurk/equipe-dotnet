﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interfaces
{
    public interface IAppTarefa : IAppGeneric<Tarefa>
    {
        void Delete(int id);
    }
}
