﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class Tarefa
    {
        public int id { get; set; }
        public string nome { get; set; }
        public bool concluida { get; set; }

        public int usuarioId { get; set; }
        //[NotMapped]
        //[ForeignKey("usuarioId")]
        //public virtual Usuario usuario { get; set; }

        public int listaId { get; set; }
        //[NotMapped]
        //[ForeignKey("listaId")]
        //public virtual Lista lista { get; set; }

        public Tarefa()
        {

        }
    }
}
