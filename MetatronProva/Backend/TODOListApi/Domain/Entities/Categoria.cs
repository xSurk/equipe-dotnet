﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class Categoria
    {
        public int id { get; set; }
        public string nome { get; set; }

        //[NotMapped]
        //public virtual ICollection<Lista> listas { get; set; }

        public Categoria()
        {

        }
    }
}
