﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class Usuario
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        public string senha { get; set; }

        //[NotMapped]
        //public virtual ICollection<Tarefa> tarefas { get; set; }

        public Usuario()
        {

        }
    }
}
