﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class Lista
    {
        public int id { get; set; }
        public string nome { get; set; }        
        public bool concluida { get; set; }
        public int categoriaId { get; set; }        

        //[NotMapped]
        //public virtual ICollection<Tarefa> tarefas { get; set; }

        public Lista()
        {

        }
    }
}
