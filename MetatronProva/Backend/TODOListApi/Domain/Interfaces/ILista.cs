﻿using Domain.Entities;
using Domain.Interfaces.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces
{
    public interface ILista : InterfaceGeneric<Lista>
    {
    }
}
