﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Interfaces;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace TODOListMetatron.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TarefaController : ControllerBase
    {
        private readonly IAppTarefa AppTarefa;

        public TarefaController(IAppTarefa _AppTarefa)
        {
            AppTarefa = _AppTarefa;
        }

        // GET: api/Pessoa
        [HttpGet]
        public IEnumerable<Tarefa> List()
        {
            return AppTarefa.List();
        }

        // GET: api/Pessoa/5
        [HttpGet("{id}")]
        public Tarefa Get(int id)
        {
            return AppTarefa.Get(id);
        }

        // POST: api/Pessoa
        [HttpPost]
        public void Post([FromBody] Tarefa tarefa)
        {
            AppTarefa.Add(tarefa);
        }

        // PUT: api/Pessoa/5
        [HttpPut]
        public void Put([FromBody] Tarefa tarefa)
        {
            AppTarefa.Update(tarefa);
        }

        // DELETE: api/ApiWithActions/5
        [HttpGet("Delete/{id}")]
        public void Delete(int id)
        {
            AppTarefa.Delete(id);
        }
    }
}
