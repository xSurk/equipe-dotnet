﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Interfaces;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace TODOListMetatron.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : Controller
    {
        private readonly IAppUsuario AppUsuario;

        public UsuarioController(IAppUsuario _AppUsuario)
        {
            AppUsuario = _AppUsuario;
        }

        // GET: api/Pessoa
        [HttpGet]
        public IEnumerable<Usuario> List()
        {
            return AppUsuario.List();
        }

        // GET: api/Pessoa/5
        [HttpGet("{id}")]
        public Usuario Get(int id)
        {
            return AppUsuario.Get(id);
        }

        [HttpGet("Login")]
        public Usuario Login(string nome, string senha)
        {
            return AppUsuario.Login(nome,senha);
        }

        // POST: api/Pessoa
        [HttpPost]
        public void Post([FromBody] Usuario usuario)
        {
            AppUsuario.Add(usuario);
        }

        // PUT: api/Pessoa/5
        [HttpPut("{id}")]
        public void Put([FromBody] Usuario usuario)
        {
            AppUsuario.Update(usuario);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            AppUsuario.Delete(id);
        }

    }
}