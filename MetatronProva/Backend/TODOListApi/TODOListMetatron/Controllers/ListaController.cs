﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Interfaces;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace TODOListMetatron.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ListaController : ControllerBase
    {
        private readonly IAppLista AppLista;

        public ListaController(IAppLista _AppLista)
        {
            AppLista = _AppLista;
        }

        // GET: api/Pessoa
        [HttpGet]
        public IEnumerable<Lista> List()
        {
            return AppLista.List();
        }

        // GET: api/Pessoa/5
        [HttpGet("{id}")]
        public Lista Get(int id)
        {
            return AppLista.Get(id);
        }

        // POST: api/Pessoa
        [HttpPost]
        public void Post([FromBody] Lista lista)
        {
            AppLista.Add(lista);
        }

        // PUT: api/Pessoa/5
        [HttpPut]
        public void Put([FromBody] Lista lista)
        {
            AppLista.Update(lista);
        }

        // DELETE: api/ApiWithActions/5
        [HttpGet("Delete/{id}")]
        public void Delete(int id)
        {
            AppLista.Delete(id);
        }
    }
}
