﻿using System.Collections.Generic;
using Application.Interfaces;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace TODOListMetatron.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriaController : ControllerBase
    {
        private readonly IAppCategoria AppCategoria;

        public CategoriaController(IAppCategoria _AppCategoria)
        {
            AppCategoria = _AppCategoria;
        }

        // GET: api/Pessoa
        [HttpGet]
        public IEnumerable<Categoria> List()
        {
            return AppCategoria.List();
        }

        // GET: api/Pessoa/5
        [HttpGet("{id}")]
        public Categoria Get(int id)
        {
            return AppCategoria.Get(id);
        }

        // POST: api/Pessoa
        [HttpPost]
        public void Post([FromBody] Categoria categoria)
        {
            AppCategoria.Add(categoria);
        }

        // PUT: api/Pessoa/5
        [HttpPut("{id}")]
        public void Put([FromBody] Categoria categoria)
        {
            AppCategoria.Update(categoria);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            AppCategoria.Delete(id);
        }
    }
}
