﻿using Application.Applications;
using Application.Interfaces;
using Domain.Interfaces;
using Domain.Interfaces.Generic;
using Infra.Config;
using Infra.Repositories;
using Infra.Repositories.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace TODOListMetatron
{
    public class Startup
    {
        readonly string corsOrigins = "_corsOrigins";
        public Startup(IConfiguration configuration)
        {            
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Configura quais origens são confiáveis ao CORS
            services.AddCors(options => {
                options.AddPolicy(corsOrigins,
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:4200")
                                .AllowAnyMethod()
                                .AllowAnyHeader();                                
                    });
            });

            var conn = Configuration.GetConnectionString("TODOList");

            services.AddDbContext<TODOListContext>(option => option.UseLazyLoadingProxies()
                                                .UseMySql(conn, m => m.MigrationsAssembly("Infra"))   //Informa o projeto no qual contém as informações para o Entity Framework                                 
            );

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSingleton(typeof(InterfaceGeneric<>), typeof(RepositoryGeneric<>));
            services.AddScoped<IUsuario, RepositoryUsuario>();
            services.AddScoped<IAppUsuario, ApplicationUsuario>();
            services.AddScoped<ICategoria, RepositoryCategoria>();
            services.AddScoped<IAppCategoria, ApplicationCategoria>();
            services.AddScoped<ILista, RepositoryLista>();
            services.AddScoped<IAppLista, ApplicationLista>();
            services.AddScoped<ITarefa, RepositoryTarefa>();
            services.AddScoped<IAppTarefa, ApplicationTarefa>();

            services.AddSwaggerGen(s => {
                s.SwaggerDoc("v1", 
                    new Microsoft.OpenApi.Models.OpenApiInfo {
                                Title = "TODO List API",
                                Description = "Swagger para a api de Lista de Afazeres",
                                Contact = new Microsoft.OpenApi.Models.OpenApiContact
                                {
                                    Name = "Renato Bessa da Paz",
                                    Url = new System.Uri("https://github.com/xSurk")
                                }
                    });
            });

            services.AddMvcCore();            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors(corsOrigins); //Injeta URLs para o CORS
            app.UseHttpsRedirection();
            app.UseMvc();            

            app.UseSwagger();
            app.UseSwaggerUI(s => {
                s.SwaggerEndpoint("./v1/swagger.json", "v1 docs");
            });
        }
    }
}
