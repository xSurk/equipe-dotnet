﻿using Domain.Entities;
using Domain.Interfaces;
using Infra.Config;
using Infra.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Repositories
{
    public class RepositoryTarefa : RepositoryGeneric<Tarefa>, ITarefa
    {
        private readonly TODOListContext repo;
        public RepositoryTarefa(TODOListContext context) : base(context)
        {
            repo = context;
        }

        public new void Update(Tarefa entity)
        {
            using (repo)
            {
                var tarefa = repo.Set<Tarefa>().FirstOrDefault(x => x.id == entity.id);

                tarefa.concluida = entity.concluida;
                tarefa.nome = entity.nome;                

                repo.Set<Tarefa>().Update(tarefa);
                repo.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (repo)
            {
                var tarefa = repo.Set<Tarefa>().FirstOrDefault(x => x.id == id);

                repo.Set<Tarefa>().Remove(tarefa);
                repo.SaveChanges();
            }
        }

    }
}
