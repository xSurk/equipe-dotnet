﻿using Domain.Entities;
using Domain.Interfaces;
using Infra.Config;
using Infra.Repositories.Generic;
using System.Linq;

namespace Infra.Repositories
{
    public class RepositoryUsuario : RepositoryGeneric<Usuario>, IUsuario
    {
        private readonly TODOListContext repo;
        public RepositoryUsuario(TODOListContext context) : base(context)
        {
            repo = context;
        }

        public Usuario Login(string nome, string senha)
        {
            using (repo)
            {
                //Usuario user = repo.Usuarios.FirstOrDefault(x => x.nome == nome && x.senha == senha);

                return repo.Usuarios.FirstOrDefault(x => x.nome == nome && x.senha == senha);
            }
        }
    }
}
