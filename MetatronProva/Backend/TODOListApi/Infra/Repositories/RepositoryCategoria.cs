﻿using Domain.Entities;
using Domain.Interfaces;
using Infra.Config;
using Infra.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Repositories
{
    public class RepositoryCategoria : RepositoryGeneric<Categoria>, ICategoria
    {
        public RepositoryCategoria(TODOListContext context) : base(context)
        {
        }
    }
}
