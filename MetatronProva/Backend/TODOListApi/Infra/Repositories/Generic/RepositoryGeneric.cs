﻿using Domain.Interfaces.Generic;
using Infra.Config;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Repositories.Generic
{
    public class RepositoryGeneric<T> : InterfaceGeneric<T>, IDisposable where T : class
    {
        private readonly TODOListContext repo;

        public RepositoryGeneric(TODOListContext context)
        {
            repo = context;
        }

        public void Add(T entity)
        {
            using (repo)
            {
                repo.Set<T>().Add(entity);
                repo.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (repo)
            {
                //repo.Set<T>().Remove(entity);
                //repo.SaveChanges();
            }
        }

        public T Get(int id)
        {
            using (repo)
            {
                return repo.Set<T>().Find(id);                
            }
        }

        public List<T> List()
        {
            using (repo)
            {
                return repo.Set<T>().AsNoTracking().ToList();
            }
        }

        public void Update(T entity)
        {
            using (repo)
            {
                repo.Set<T>().Update(entity);
                repo.SaveChanges();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool isDisposed)
        {
            if (!isDisposed) return;
        }

        ~RepositoryGeneric()
        {

        }
    }
}
