﻿using Domain.Entities;
using Domain.Interfaces;
using Infra.Config;
using Infra.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Repositories
{
    public class RepositoryLista : RepositoryGeneric<Lista>, ILista
    {
        private readonly TODOListContext repo;
        public RepositoryLista(TODOListContext context) : base(context)
        {
            repo = context;
        }

        public new void Delete(int id)
        {
            using (repo)
            {
                var tarefa = repo.Set<Lista>().FirstOrDefault(x => x.id == id);

                repo.Set<Lista>().Remove(tarefa);
                repo.SaveChanges();
            }
        }
    }
}
