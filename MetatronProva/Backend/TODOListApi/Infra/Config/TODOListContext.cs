﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infra.Config
{
    public class TODOListContext : DbContext
    {

        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Tarefa> Tarefas { get; set; }
        public DbSet<Lista> Listas { get; set; }

        public TODOListContext(DbContextOptions<TODOListContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Categoria>().HasKey(x => x.id);
            modelBuilder.Entity<Lista>().HasKey(x => x.id);
            modelBuilder.Entity<Tarefa>().HasKey(x => x.id);
            modelBuilder.Entity<Usuario>().HasKey(x => x.id);

            //modelBuilder.Entity<Categoria>()                        
            //            .HasMany(x => x.listas)
            //            .WithOne(x => x.categoria);

            //modelBuilder.Entity<Lista>()
            //            .HasOne(x => x.categoria);
                        //.WithMany(x => x.listas);
                        

            //modelBuilder.Entity<Tarefa>()
            //            .HasOne(x => x.lista)
            //            .WithMany(x => x.tarefas);

            //modelBuilder.Entity<Tarefa>()
            //            .HasOne(x => x.usuario)
            //            .WithMany(x => x.tarefas);

            //modelBuilder.Entity<Usuario>()
            //            .HasMany(x => x.tarefas)
            //            .WithOne(x => x.usuario);
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{

        //}

    }
}
