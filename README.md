# Prova da Equipe .NET

### BackEnd

# Tecnologias Backend

 - Entity Framework Core
 - Asp.Net Core 2.2
 - Swagger
 - Pomello EntityFrameworkCore MySql

# Instruções Backend

Antes de iniciar favor verificar a string de conexão do banco mysql no arquivo appsettings.json.

Em seguida no Package Manager Console selecionar o projeto padrão como Infra e executar o comando Add-Migration 'init' e Update-Database para o entity gerar o banco.

### Observações Backend

- Swagger é um excelente auxílio na documentação e teste da API, por isso sua escolha é sempre prioridade em meus projetos.
- Entity Framework Core também é uma ferramenta que facilita a criação de um banco sem a necessidade de escrita de código SQL, o que agiliza a criação de meus projetos.
- Pomello EntityFrameworkCore MySql é uma biblioteca que fornece a conexão do Entity com o MySql, uma biblioteca indispensável para se utilizar caso escolha o banco para mysql.
- MySql é um banco leve e de fácil manutenção e visualização, por isso optei pela utilização do mesmo.
- Asp.Net Core 2.2 uma das melhores API's do mercado, a escolha de sua versão se dá pela estabilidade que a versão possui e pelo fato de eu já ter trabalhado com o mesmo.


### FrontEnd

# Tecnologias Frontend

 - Angular
 - Angular Material

# Instruções Frontend

Antes de tudo rodar o comando "npm i" no local do projeto.

Iniciar a API antes de abrir o projeto frontend, após verificar a execução da API execute o comando "ng s" no local do projeto, ou no console do VS Code. 

### Observações Frontend

- Angular é um ótimo framework para criar SPA e é bem fácil de se configurar e se ambientar com o controle de dados.
- Angular Material foi utilizado pois entrega diversos componentes compatíveis com o Angular e agiliza bastante no desenvolvimento de um projeto.


